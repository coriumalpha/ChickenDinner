﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arena.Entities
{
    class Peso
    {
        public double Victoria { get; set; }

        public double Empate { get; set; }

        public double Derrota { get; set; }

        public double getByResult(int result)
        {
            switch (result)
            {
                case -1:
                    return Derrota;
                case 0:
                    return Empate;
                case 1:
                    return Victoria;
                default:
                    throw new Exception(String.Format("Bad result ({0})", result.ToString()));
            }
        }
    }
}
