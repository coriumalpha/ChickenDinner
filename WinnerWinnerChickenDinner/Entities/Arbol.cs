﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arena.Entities;

namespace Arena
{
    class Arbol
    {
        #region Constructor
        public Arbol(int nivel, int profundidad)
        {
            this.Peso = new Peso();

            this.profundidad = profundidad;
            this.nivel = nivel;

            if (nivel < profundidad)
            {
                Hijos = new List<Arbol>();

                for (int i = 0; i < 3; i++)
                {
                    Arbol hijo = new Arbol(nivel + 1, profundidad);
                    //Una identidad por cada valor de i
                    hijo.Identidad = i;
                    hijo.Padre = this;

                    Hijos.Add(hijo);
                }
            }
        }
        #endregion  

        private int profundidad;
        public int Profundidad
        {
            get
            {
                return profundidad;
            }
        }

        private int nivel;
        public int Nivel
        {
            get
            {
                return nivel;
            }
        }
        public int Identidad { get; set; }
        public Peso Peso { get; set; }
        public Arbol Padre { get; set; }
        public ICollection<Arbol> Hijos { get; set; }

    }
}