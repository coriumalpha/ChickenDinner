﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arena.Helpers;

namespace Arena
{
    public class JugadorRandom : IJugador
    {
        public string Name { get; set; }
        int victorias = 0;
        private Random rand;

        public JugadorRandom()
        {
            rand = new Random(Guid.NewGuid().GetHashCode());
        }
        public int Victorias
        {
            get
            {
                return victorias;
            }
        }
        public char Jugar()
        {
            return NotationAdapter.toCharNotation(rand.Next(0, 3));
        }

        public void RegistrarUltimoResultado(char oponente, int resultado) { if (resultado == 1) { victorias++; } }

        public void MostrarTraza() { Console.WriteLine(); }
    }
}
