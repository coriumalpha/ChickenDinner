﻿using Arena.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arena
{
    public class JugadorBasico : IJugador
    {
        private Random rand;
        public JugadorBasico(string name)
        {
            Name = name;
            rand = new Random(Guid.NewGuid().GetHashCode());
        }

        public string Name { get; set; }

        private int? jugadaPreviaRojo = null;
        private int? jugadaPreviaAzul = null;
        private int? resultadoPrevio = null;

        private int victorias = 0;
        private int derrotas = 0;
        private int empates = 0;

        private int? ultimoModelo = null;

        private int[] puntuacionModelos = new int[3] { 200, 10, 10 };

        public int Victorias
        {
            get
            {
                return victorias;
            }
        }

        public char Jugar()
        {
            int eleccion;

            int selectorModelo = rand.Next(0, puntuacionModelos[0] + puntuacionModelos[1] + puntuacionModelos[2] + 1);

            if (selectorModelo < puntuacionModelos[0])
            {
                ultimoModelo = 0;
                eleccion = ModeloA();
            }
            else if (selectorModelo < (puntuacionModelos[0] + puntuacionModelos[1]))
            {
                ultimoModelo = 1;
                eleccion = ModeloB();
            }
            else
            {
                ultimoModelo = 2;
                eleccion = ModeloC();
            }

            return NotationAdapter.toCharNotation(eleccion);
        }

        /*
        Si gana repite, si pierde cambia, si empata 50%50
        */
        private int ModeloA()
        {
            if (jugadaPreviaRojo == null) //Iniciar
            {
                jugadaPreviaAzul = rand.Next(0, 3);
            }
            else
            {
                if (resultadoPrevio == -1) //Perder
                {
                    //Cambiar jugada
                    jugadaPreviaAzul = (((int)jugadaPreviaAzul + rand.Next(1, 3)) % 3);
                }
                else if (resultadoPrevio == 0) //Empatar
                {
                    if (rand.Next(0, 2) == 1) //Prob. 50% cambiar jugada
                    {
                        jugadaPreviaAzul = (((int)jugadaPreviaAzul + rand.Next(1, 3)) % 3);
                    }
                }
            }

            //Si no ha cambiado, se repite la jugada.
            return (int)jugadaPreviaAzul;
        }

        /*
        Devuelve jugada aleatoria  
        */
        private int ModeloB()
        {
            jugadaPreviaAzul = rand.Next(0, 3);
            return (int)jugadaPreviaAzul;
        }

        /*
        Devuelve elección aleatoria entre los valores distintos a los propuestos por el Modelo B
        */
        private int ModeloC()
        {
            jugadaPreviaAzul = ((ModeloA() + rand.Next(1, 3)) % 3);
            return (int)jugadaPreviaAzul;
        }

        public void RegistrarUltimoResultado(char charOponente, int resultado)
        {
            int oponente = NotationAdapter.toIntNotation(charOponente);

            if (resultado == 1)
            {
                puntuacionModelos[(int)ultimoModelo]++;
                victorias++;
            }
            else if (resultado == 0)
            {
                empates++;
            }
            else
            {
                derrotas++;
            }

            jugadaPreviaRojo = oponente;
            resultadoPrevio = resultado;
        }
    }
}
