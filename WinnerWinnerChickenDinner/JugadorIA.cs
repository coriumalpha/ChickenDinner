﻿using Arena.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arena
{

    /***
     * Partimos de tres nodos lvl 0 "Yo he sacado A", "Yo he sacado B", "Yo he sacado C".
     * Cada nodo registrará en uno de sus hijos la decisión más ventajosa que se
     * hubiera podido aplicar en esa partida indistintamente del resultado obtenido
     * 
     * Esto es, "Yo saco tijeras" tiene mayor tendencia a la victoria si es sucedido por "Yo saco papel"
     * Nótese que la decisión del rival no parece afectar al modelo... D:
     * 
     * "Antes he sacado papel[Ti] y he perdido, luego tijeras[Pi] y he perdido, luego tijeras[Pa] y he ganado"
     * De tal forma que si la secuencia Xn comienza por [Papel vs Tijeras], la tendencia ganadora sería sacar después papel
     * tras esto, continuar por el árbol [1, Papel-Tijeras] -> [2, Tijeras-Papel] -> Recoger tendencia de casos [tipo 3]
     * anteriores dentro de esta misma línea de juego para evaluar mejores resultados
     * 
     * A mayor profundidad del árbol, mayor definición en la captura de tendencias, si bien se incrementa exponencialmente
     * a razón de (nº Hijos último nodo = base: 3, exponente: profundidad).
     * También se puede utilizar el promedio entre varios nodos ante duda o carencia de datos...
     ***/

    public class JugadorIA : IJugador
    {
        #region Public Class Members
        public string Name { get; set; }

        public int Victorias { get; private set; } = 0;
        #endregion

        #region Private Class Members
        private int rondasJugadas
        {
            get
            {
                return historial.Length;
            }
        }

        private int[] historial = new int[] { };

        private int ultimaIdentidad;

        private int profundidad = 4;
        #endregion

        #region Constructor

        private Arbol _Arbol;
        public JugadorIA()
        {
            _Arbol = new Arbol(0, profundidad);
        }
        #endregion

        public char Jugar()
        {
            //Esto es el punto de entrada de una clase que cargará mucha lógica
            //sería conveniente desacoplar el resto del código de este método.

            int identidad = -99999;

            if (_Arbol.Hijos.Any())
            {
                identidad = _Arbol.Hijos.OrderByDescending(h => h.Peso).First().Identidad;
            }

            ultimaIdentidad = identidad;
            return NotationAdapter.toCharNotation(identidad);
        }

        public void RegistrarUltimoResultado(char charOponente, int resultado)
        {
            int oponente = NotationAdapter.toIntNotation(charOponente);

            if (resultado == 1)
            {
                Victorias++;
            }
        }

        public void MostrarTraza() { Console.WriteLine(); }
    }
}
