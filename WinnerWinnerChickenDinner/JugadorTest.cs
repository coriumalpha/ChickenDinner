﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arena
{
    public class JugadorTest : IJugador
    {
        public string Name { get; set; }
        int victorias = 0;
        private char respuesta;
        public JugadorTest(char r)
        {
            respuesta = r;
        }
        public int Victorias
        {
            get
            {
                return victorias;
            }
        }
        public char Jugar()
        {
            return respuesta;
        }

        public void RegistrarUltimoResultado(char oponente, int resultado) { if (resultado == 1) { victorias++; } }

        public void MostrarTraza() { Console.WriteLine(); }
    }
}
