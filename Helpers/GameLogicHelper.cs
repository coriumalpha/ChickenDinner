﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arena.Helpers
{
    public static class GameLogicHelper
    {
        public static int evaluate(char pa, char pb)
        {
            int playerA = NotationAdapter.toIntNotation(pa);
            int playerB = NotationAdapter.toIntNotation(pb);
            //Función optimizada por Jorge.
            //¡¡¡No se ha probado su integración!!!
            int val = (playerA - playerB + 3) % 3;
            return (val == 2) ? -1 : val;
        }

        public static void imprimir(int pa, int pb, int resultado)
        {
            List<string> hr = new List<string>
            {
                { "Piedra" },
                { "Tijera" },
                { "Papel" }
            };

            List<string> lResultados = new List<string>
            {
                { "Derrota" },
                { "Empate" },
                { "Victoria" }
            };

            string strResultado = lResultados[resultado + 1];

            string result = hr[pa] + " - " + hr[pb] + " -> " + strResultado;
            Console.WriteLine(result);
        }
    }
}
