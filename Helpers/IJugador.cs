﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arena
{
    public interface IJugador
    {
        int Victorias { get; }

        char Jugar();

        void RegistrarUltimoResultado(char jugadorB, int resultado);
    }
}
