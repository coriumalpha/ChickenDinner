﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arena.Helpers
{
    public static class NotationAdapter
    {
        public static int toIntNotation(char c)
        {
            String s = "rspRSP";

            try
            {
                return (s.IndexOf(c) % 3);
            }
            catch
            {
                throw new Exception(String.Format("Char '{0}' not in list", c));
            }
        }

        public static char toCharNotation(int n, bool isUppercase = false)
        {
            String s = "rsp";
            return ((isUppercase) ? s.ToUpper() : s)[n];
        }
    }
}
