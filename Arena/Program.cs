﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arena.Helpers;

namespace Arena
{
    class Program
    {
        static void Main(string[] args)
        {
            Entry();
        }

        /*
         * Este método efectua las veces de punto de entrada de la aplicación
         * Abstenerse de escribir lógica en el método Main()
         */
        static private void Entry()
        {
            const int partidas = 50000;

            IJugador Azul = new JugadorBasico("Azul");
            IJugador Rojo = new JugadorRandom();
            Partida(partidas, Rojo, Azul);

            Console.ReadKey();
        }

        static private void Partida(int partidas, IJugador Azul, IJugador Rojo)
        {
            int victoriasAzul = 0;
            int victoriasRojo = 0;

            for (int x = 0; x < partidas; x++)
            {
                int resultado = Jugada(Azul, Rojo);

                if (resultado == 1)
                {
                    victoriasAzul++;
                }
                else if (resultado == -1)
                {
                    victoriasRojo++;
                }
            }

            Console.WriteLine(String.Format("Azul {0} - {1} Rojo", victoriasAzul, victoriasRojo));
        }

        static private int Jugada(IJugador Azul, IJugador Rojo)
        {
            char jugadaAzul = Azul.Jugar();
            char jugadaRojo = Rojo.Jugar();

            int resultado = GameLogicHelper.evaluate(jugadaAzul, jugadaRojo);

            //Nótese que el resultado Rojo es asignado al jugador Azul y viceversa.
            Azul.RegistrarUltimoResultado(jugadaRojo, resultado);
            Rojo.RegistrarUltimoResultado(jugadaAzul, resultado * (-1));

            return resultado;
        }
    }
}
